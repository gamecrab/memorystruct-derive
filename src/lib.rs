extern crate proc_macro;

use proc_macro::TokenStream;
use syn::{parse_macro_input, DeriveInput};
use quote::quote;


#[proc_macro_derive(MemoryStruct, attributes(offset))]
pub fn derive(input: TokenStream) -> TokenStream
{
    let ast = parse_macro_input!(input as DeriveInput);

    let name = ast.ident.to_string();
    

    for attribute in &ast.attrs
    {
        match attribute.parse_meta().unwrap(){
            syn::Meta::Path(_) => (),
            syn::Meta::List(_) => (),
            syn::Meta::NameValue(value) => {

            }
        }
    }


    let implementation = quote!{
        impl std::iter::Iterator for #name
        {

            pub enum ValueType 
            {
                USize(usize),
                Str(String),
                Bool(bool),
            } 

            type Item = ValueType;

            pub fn next(&mut self) -> Option<Self::Item>

        }
    };

    implementation.into()
}